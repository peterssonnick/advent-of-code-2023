from main import (
    parse_time,
    parse_distance,
    calculate_speed_mm_per_second,
    calculate_travel_distance,
    determine_winning_wait_times
)


def test_parse_time():
    line = 'Time:      7  15   30'
    expected = [7, 15, 30]
    actual = parse_time(line)
    assert actual == expected


def test_parse_distance():
    distance = 'Distance:  9  40  200'
    expected = [9, 40, 200]
    actual = parse_distance(distance)
    assert actual == expected


def test_calculate_speed_mm_per_second():
    assert calculate_speed_mm_per_second(hold_time=1) == 1
    assert calculate_speed_mm_per_second(hold_time=2) == 2
    assert calculate_speed_mm_per_second(hold_time=5) == 5


def test_calculate_travel_distance():
    assert calculate_travel_distance(time=7, speed=1, wait=1) == 6
    assert calculate_travel_distance(time=7, speed=2, wait=2) == 10
    assert calculate_travel_distance(time=7, speed=6, wait=6) == 6
    assert calculate_travel_distance(time=7, speed=7, wait=7) == 0
    assert calculate_travel_distance(time=7, speed=8, wait=8) == 0


def test_determine_winning_wait_times():
    assert determine_winning_wait_times(race_duration=7, winning_distance=9) == [2, 3, 4, 5]
    assert determine_winning_wait_times(race_duration=15, winning_distance=40) == [4, 5, 6, 7, 8, 9, 10, 11]
    assert determine_winning_wait_times(race_duration=30, winning_distance=200) == [11, 12, 13, 14, 15, 16, 17, 18, 19]
