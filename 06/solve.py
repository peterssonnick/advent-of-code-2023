from main import solve

with open('./inputs/input_pt2.txt') as f:
    lines = f.readlines()
    solution = solve(lines[0], lines[1])

for i in solution:
    # print(i)
    print(len(i))