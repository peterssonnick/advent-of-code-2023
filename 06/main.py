def parse_time(line):
    return [int(time) for time in line.split('Time:')[1].strip().split(' ') if time != '']


def parse_distance(line):
    return [int(time) for time in line.split('Distance:')[1].strip().split(' ') if time != '']


def calculate_speed_mm_per_second(hold_time):
    return hold_time * 1


def calculate_travel_distance(time, speed, wait):
    return speed * (time - wait) if wait < time else 0


def determine_winning_wait_times(race_duration, winning_distance):
    winning_wait_times = []
    for i in range(race_duration):
        wait_time = i
        speed = calculate_speed_mm_per_second(i)
        distance = calculate_travel_distance(race_duration, speed, wait_time)

        if distance > winning_distance:
            winning_wait_times.append(wait_time)

    return winning_wait_times


def solve(times_line, distances_lines):
    times = parse_time(times_line)
    distances = parse_distance(distances_lines)
    winners = []

    for i in range(len(times)):
        time = times[i]
        distance = distances[i]
        winning_times = determine_winning_wait_times(time, distance)
        winners.append(winning_times)

    return winners
