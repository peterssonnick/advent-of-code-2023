from main import (
    parse_node,
    split_left_right,
    get_next_node,
    identify_starting_nodes
)


def test_split_left_right():
    lr = '(BBB, BBB)'
    expected = ['BBB', 'BBB']
    actual = split_left_right(lr)
    assert actual == expected


def test_parse_node():
    line = 'GGQ = (MVX, XBP)\n'
    expected = {'GGQ': ['MVX', 'XBP']}
    actual = parse_node(line)
    assert actual == expected


def test_get_next_node():
    node_values = ['MVX', 'XBP']
    assert get_next_node(node_values, 'L') == 'MVX'
    assert get_next_node(node_values, 'R') == 'XBP'


def test_identify_starting_nodes():
    nodes = {
        '11A': ['11B', 'XXX'],
        '22A': ['22B', 'XXX'],
        '22B': ['22C', '22C']
    }
    expected = ['11A', '22A']
    actual = identify_starting_nodes(nodes)
    assert actual == expected
