from main import parse_node, count_steps_to_zzz_node, identify_starting_nodes
import math

with open('./inputs/input.txt') as f:
    lines = f.readlines()
    instructions = lines[0].strip()
    start = 'AAA'
    nodes = {}

    for line in lines[2:]:
        node = parse_node(line)
        nodes = {**node, **nodes}


# steps = count_steps_to_zzz_node(instructions, nodes, [start])
# print(f'pt. 1: {steps}')

# starting_nodes = identify_starting_nodes(nodes)
# for starting_node in starting_nodes[5:]:
#     print(f'starting node: {starting_node}')
#     steps = count_steps_to_zzz_node(instructions, nodes, [starting_node])
# print(f'pt. 2: {steps}')

# BPA: 14257, 25017, 35777 = (10,760)
# VJA: 19099, 33087, 47075 = (13,988)
# DXA: 16409, 22327, 28245 = (5,918)
# VGA: 12643, 17485, 22327 = (4,842)
# BFA: 21251, 32549, 43847 = (11,298)
# AAA: 11567, 12643, 13719 = (1,076)
bpa_loop = 10760
vja_loop = 13988
dxa_loop = 5918
vga_loop = 4842
bfa_loop = 11298
aaa_loop = 1076

max_start = 21251
max_loop = vja_loop

bpa = (bpa_loop - max_start)
vja = (max_start - 19099)
dxa = (max_start - 16409)
vga = (max_start - 12643)
bfa = (max_start - 21251)
aaa = (max_start - 11567)

complete = False
i = 0
steps = max_start

# lcm_result = math.lcm(bpa_loop, vja_loop, dxa_loop, vga_loop, bfa_loop, aaa_loop)
# print(f'lcm_result: {lcm_result}')


while not complete:
    if steps % bpa_loop == 0 and steps % vja_loop == 0 and steps % dxa_loop == 0 and steps % vga_loop == 0 and steps & bfa_loop == 0 and steps % aaa_loop == 0:
        print('done')
        complete = True
    else:
        steps += max_loop

    if i % 10000 == 0:
        print(steps)
    i += 1
#
# print(f'total: {bfa + max_start}')


# example_a: 100, 102, 104, 106, 188
# example_b: 3, 6, 9, 12... 99, 102