def split_left_right(lr):
    [left, right] = lr.split(', ')
    return [left[1:], right.strip()[:-1]]


def parse_node(line):
    [key, lr] = line.split(' = ')
    [left, right] = split_left_right(lr)
    return {key: [left, right]}


def get_next_node(node_values, direction):
    return node_values[0] if direction == 'L' else node_values[1]


def identify_starting_nodes(nodes):
    return [key for key in nodes.keys() if key[2] == 'A']


def check_if_all_keys_end_in_char(keys, char):
    for key in keys:
        if key[2] != char:
            return False
    return True


def count_steps_to_zzz_node(instructions, nodes, starting_nodes):
    found_z = False
    complete = False
    current_keys = starting_nodes
    i = 1
    number_of_steps = 0

    while not complete:
        # print(f'{i} :: begin')
        for j, direction in enumerate(instructions):
            new_keys = []
            for key in current_keys:
                next_key = get_next_node(nodes.get(key), direction)
                new_keys.append(next_key)
            if check_if_all_keys_end_in_char(new_keys, 'Z'):
                number_of_steps = i * (j + 1)
                print(f'made it in to Z in {number_of_steps} moves')
                found_z = True
            else:
                current_keys = new_keys

            if found_z and check_if_all_keys_end_in_char(new_keys, 'A') and direction == 'L':
                number_of_steps = i * (j + 1)
                print(f'made it back to A in {number_of_steps} moves')
                complete = True
                break
        i += 1

    return number_of_steps
