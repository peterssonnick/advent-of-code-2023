from main import (
    split_game_id_from_game_sets,
    split_game_sets,
    split_color_counts_from_game_set,
    split_count_from_color,
    sum_color_numbers_from_game_set,
    determine_if_game_is_legal,
    return_game_id_if_game_is_legal,
    return_power_of_minimum_needed_for_legal_game
)


def test_can_parse_game_id_from_results():
    game_string = 'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green'
    expected = [1, '3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green']
    actual = split_game_id_from_game_sets(game_string)
    assert actual == expected


def test_can_parse_game_id_from_results_when_game_id_is_99():
    game_string = 'Game 99: 1 red, 4 blue; 5 red, 8 blue; 3 blue, 1 green; 2 red, 6 blue; 8 blue, 2 green, 3 red'
    expected = [99, '1 red, 4 blue; 5 red, 8 blue; 3 blue, 1 green; 2 red, 6 blue; 8 blue, 2 green, 3 red']
    actual = split_game_id_from_game_sets(game_string)
    assert actual == expected


def test_can_parse_game_id_from_results_when_game_id_is_100():
    game_string = 'Game 100: 5 green, 1 red; 4 blue, 8 red, 4 green; 1 blue, 3 red, 15 green; 1 blue, 15 green, 1 red; 2 red, 13 green'
    expected = [100, '5 green, 1 red; 4 blue, 8 red, 4 green; 1 blue, 3 red, 15 green; 1 blue, 15 green, 1 red; 2 red, 13 green']
    actual = split_game_id_from_game_sets(game_string)
    assert actual == expected


def test_can_split_game_sets():
    game_sets = '3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green'
    expected = ['3 blue, 4 red', '1 red, 2 green, 6 blue', '2 green']
    actual = split_game_sets(game_sets)
    assert actual == expected


def test_can_split_color_counts_from_game_set():
    game_set = '3 blue, 4 red'
    expected = ['3 blue', '4 red']
    actual = split_color_counts_from_game_set(game_set)
    assert actual == expected


def test_can_split_count_from_color():
    color_count = '3 blue'
    expected = [3, 'blue']
    actual = split_count_from_color(color_count)
    assert actual == expected


def test_can_sum_color_numbers_from_game_set():
    game_sets = '3 blue, 4 red'
    expected = {'blue': 3, 'red': 4}
    actual = sum_color_numbers_from_game_set(game_sets)
    assert actual == expected


def test_returns_true_if_game_is_legal():
    max_red = 12
    max_blue = 13
    max_green = 14
    game_sets = {'blue': 3, 'red': 4}
    expected = True
    actual = determine_if_game_is_legal(max_red, max_blue, max_green, game_sets)
    assert actual == expected


def test_returns_false_if_game_is_illegal():
    max_red = 12
    max_blue = 13
    max_green = 14
    game_sets = {'green': 8, 'blue': 16, 'red': 12}
    expected = False
    actual = determine_if_game_is_legal(max_red, max_blue, max_green, game_sets)
    assert actual == expected


def test_returns_game_id_if_game_is_legal():
    max_red = 12
    max_blue = 13
    max_green = 14
    game_string = 'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green'
    expected = 1
    actual = return_game_id_if_game_is_legal(max_red, max_blue, max_green, game_string)
    assert actual == expected


def test_returns_0_if_game_is_illegal():
    max_red = 12
    max_blue = 13
    max_green = 14
    game_string = 'Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red'
    expected = 0
    actual = return_game_id_if_game_is_legal(max_red, max_blue, max_green, game_string)
    assert actual == expected


def test_returns_game_id_if_game_is_legal_99():
    max_red = 12
    max_blue = 13
    max_green = 14
    game_string = 'Game 99: 1 red, 4 blue; 5 red, 8 blue; 3 blue, 1 green; 2 red, 6 blue; 8 blue, 2 green, 3 red'
    expected = 99
    actual = return_game_id_if_game_is_legal(max_red, max_blue, max_green, game_string)
    assert actual == expected


def test_returns_power_of_minimum_needed_for_legal_game_48():
    game_string = 'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green'
    expected = 48
    actual = return_power_of_minimum_needed_for_legal_game(game_string)
    assert actual == expected


def test_returns_power_of_minimum_needed_for_legal_game_1560():
    game_string = 'Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red'
    expected = 1560
    actual = return_power_of_minimum_needed_for_legal_game(game_string)
    assert actual == expected
