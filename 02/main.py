def split_game_id_from_game_sets(game_string):
    [game_id, game_sets] = game_string.split(':')
    return [int(game_id[len('Game '):]), game_sets.strip()]


def split_game_sets(game_results_string):
    return [part.strip() for part in game_results_string.split(';')]


def split_color_counts_from_game_set(game_set):
    return [color_count.strip() for color_count in game_set.split(',')]


def split_count_from_color(color_count):
    [number, color] = color_count.split(' ')
    return [int(number), color]


def sum_color_numbers_from_game_set(game_set):
    result = {}
    for color_count in split_color_counts_from_game_set(game_set):
        [count, color] = split_count_from_color(color_count)
        result[color] = count
    return result


def get_totals_for_each_color(color_number_sums):
    total_red = 0 if 'red' not in color_number_sums else color_number_sums['red']
    total_blue = 0 if 'blue' not in color_number_sums else color_number_sums['blue']
    total_green = 0 if 'green' not in color_number_sums else color_number_sums['green']
    return [total_red, total_blue, total_green]


def determine_if_game_is_legal(max_red, max_blue, max_green, color_number_sums):
    [total_red, total_blue, total_green] = get_totals_for_each_color(color_number_sums)
    return total_red <= max_red and total_blue <= max_blue and total_green <= max_green


def return_game_id_if_game_is_legal(max_red, max_blue, max_green, game_string):
    [game_id, game_sets] = split_game_id_from_game_sets(game_string)
    game_is_legal = True

    for game_set in split_game_sets(game_sets):
        color_number_sums = sum_color_numbers_from_game_set(game_set)
        if not determine_if_game_is_legal(max_red, max_blue, max_green, color_number_sums):
            game_is_legal = False
            break
    return game_id if game_is_legal else 0


def return_power_of_minimum_needed_for_legal_game(game_string):
    [game_id, game_sets] = split_game_id_from_game_sets(game_string)

    min_red_needed = 0
    min_blue_needed = 0
    min_green_needed = 0

    for game_set in split_game_sets(game_sets):
        color_number_sums = sum_color_numbers_from_game_set(game_set)
        [total_red, total_blue, total_green] = get_totals_for_each_color(color_number_sums)

        min_red_needed = total_red if total_red > min_red_needed else min_red_needed
        min_blue_needed = total_blue if total_blue > min_blue_needed else min_blue_needed
        min_green_needed = total_green if total_green > min_green_needed else min_green_needed

    return min_red_needed * min_blue_needed * min_green_needed
