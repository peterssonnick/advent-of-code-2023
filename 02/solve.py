from main import return_game_id_if_game_is_legal, return_power_of_minimum_needed_for_legal_game

MAX_RED_CUBES = 12
MAX_GREEN_CUBES = 13
MAX_BLUE_CUBES = 14

sum_of_legal_game_ids = 0
sum_of_power_of_minimums = 0

with open('./inputs/input.txt') as f:
    games = f.readlines()
    for game in games:
        sum_of_legal_game_ids += return_game_id_if_game_is_legal(MAX_RED_CUBES, MAX_BLUE_CUBES, MAX_GREEN_CUBES, game)
        sum_of_power_of_minimums += return_power_of_minimum_needed_for_legal_game(game)

print(f'result p1 :: {sum_of_legal_game_ids}')
print(f'result p2 :: {sum_of_power_of_minimums}')

