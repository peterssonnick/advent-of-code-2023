from main import rank_hands

with open('./inputs/input.txt') as f:
    lines = f.readlines()
    ranked = rank_hands(lines)

score = 0
rank = 1
for key in ranked.keys():
    print(f'{key} :: {ranked.get(key)}')
    for [cards, bid] in ranked.get(key):
        score += (bid*rank)
        rank +=1
print(score)