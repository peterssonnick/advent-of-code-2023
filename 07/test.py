from main import (
    split_cards_from_bid,
    build_card_map,
    sort_card_map_by_values,
    is_high_card,
    is_one_pair,
    is_two_pair,
    is_three_of_a_kind,
    is_full_house,
    is_four_of_a_kind,
    is_five_of_a_kind,
    get_card_rank,
    compare_cards,
    compare_hands,
    identify_index_for_new_hand,
    rank_hands
)


def test_split_cards_from_bid():
    line = '32T3K 765'
    actual = split_cards_from_bid(line)
    assert actual[0] == '32T3K'
    assert actual[1] == '765'


def test_build_card_map():
    cards = '32T3K'
    expected = {
        '3': 2,
        '2': 1,
        'T': 1,
        'K': 1
    }
    actual = build_card_map(cards)
    assert actual == expected


def test_sort_card_values():
    card_map = {'2': 1, '3': 2, 'T': 1, 'K': 1}
    expected = {'3': 2, '2': 1, 'T': 1, 'K': 1}
    actual = sort_card_map_by_values(card_map)
    assert list(actual.values()) == list(expected.values())


def test_is_high_card():
    assert is_high_card({ '3': 1, '4': 1, '2': 1, 'T': 1, 'K': 1 }) is True
    assert is_high_card({'3': 2, '2': 2, 'T': 1 }) is False
    assert is_high_card({'3': 1, '4': 1, '2': 1, 'J': 1, 'K': 1}) is False


def test_is_one_pair():
    assert is_one_pair({ '3': 2, '2': 1, 'T': 1, 'K': 1 }) is True
    assert is_one_pair({'3': 2, '2': 2, 'T': 1 }) is False
    assert is_one_pair({'3': 1, '2': 1, 'T': 1, 'K': 1, 'J': 1}) is True


def test_is_two_pair():
    assert is_two_pair({ '3': 2, '2': 1, 'T': 1, 'K': 1 }) is False
    assert is_two_pair({'3': 2, '2': 2, 'T': 1 }) is True
    assert is_two_pair({'Q': 2, '2': 1, 'T': 1, 'J': 1}) is False


def test_is_three_of_a_kind():
    assert is_three_of_a_kind({'3': 2, '2': 1, 'T': 1, 'K': 1}) is False
    assert is_three_of_a_kind({'Q': 3, '2': 1, 'T': 1}) is True
    assert is_three_of_a_kind({'Q': 2, '2': 1, 'T': 1, 'J': 1}) is True
    assert is_three_of_a_kind({'Q': 1, '2': 1, 'T': 1, 'J': 2}) is True


def test_is_full_house():
    assert is_full_house({'3': 3, '2': 2}) is True
    assert is_full_house({'Q': 3, '2': 1, 'T': 1}) is False
    assert is_full_house({'Q': 2, '2': 2, 'J': 1}) is True


def test_is_four_of_a_kind():
    assert is_four_of_a_kind({ '3': 4, '2': 1}) is True
    assert is_four_of_a_kind({'Q': 3, '2': 1, 'T': 1}) is False
    assert is_four_of_a_kind({'Q': 3, '2': 1, 'J': 1}) is True


def test_is_five_of_a_kind():
    assert is_five_of_a_kind({'3': 5}) is True
    assert is_five_of_a_kind({'Q': 3, '2': 1, 'T': 1}) is False
    assert is_five_of_a_kind({'Q': 4, 'J': 1}) is True
    assert is_five_of_a_kind({'J': 5}) is True


def test_get_card_rank():
    assert get_card_rank('J') == 0
    assert get_card_rank('2') == 1
    assert get_card_rank('A') == 12


def test_compare_cards():
    assert compare_cards('2', '3') == -1
    assert compare_cards('7', '7') == 0
    assert compare_cards('A', '7') == 1


def test_compare_hands():
    assert compare_hands('KK677', 'QQ677') == 1
    assert compare_hands('QQ677', 'KK677') == -1
    assert compare_hands('KK677', 'KTJJT') == 1
    assert compare_hands('KTJJT', 'KK677') == -1


def test_identify_index_for_hand():
    assert identify_index_for_new_hand('KK677', [['QQ677', 0], ['KTJJT', 0]]) == 2
    assert identify_index_for_new_hand('KTJJT', [['QQ677', 0], ['KK677', 0]]) == 1
    assert identify_index_for_new_hand('QQ677', [['KTJJT', 0], ['KK677', 0]]) == 0


def test_rank_hands():
    hands = [
        '32T3K 765',
        'T55J5 684',
        'KK677 28',
        'KTJJT 220',
        'QQQJA 483'
    ]
    expected = {
        0: [],
        1: [['32T3K', 765]],
        2: [['KK677', 28]],
        3: [],
        4: [],
        5: [['T55J5', 684], ['QQQJA', 483], ['KTJJT', 220]],
        6: []
    }
    actual = rank_hands(hands)
    assert actual == expected
