def split_cards_from_bid(line):
    return line.split(' ')


def build_card_map(cards):
    card_map = {}
    for card in cards:
        if card not in card_map.keys():
            card_map[card] = 1
        else:
            card_map[card] = card_map.get(card) + 1
    return card_map


def sort_card_map_by_values(card_map):
    return {k: v for k, v in sorted(card_map.items(), key=lambda item: item[1], reverse=True)}


def add_jokers(card_map):
    copy = card_map.copy()
    jokers = copy.pop('J', 0)
    scored = list(copy.values())
    if len(scored):
        scored[0] = scored[0] + jokers
    else:
        scored = [jokers]
    return scored


def is_high_card(card_map):
    scored = add_jokers(card_map)
    return scored == [1, 1, 1, 1, 1]


def is_one_pair(card_map):
    scored = add_jokers(card_map)
    return scored == [2, 1, 1, 1]


def is_two_pair(card_map):
    scored = add_jokers(card_map)
    return scored == [2, 2, 1]


def is_three_of_a_kind(card_map):
    scored = add_jokers(card_map)
    return scored == [3, 1, 1]


def is_full_house(card_map):
    scored = add_jokers(card_map)
    return scored == [3, 2]


def is_four_of_a_kind(card_map):
    scored = add_jokers(card_map)
    return scored == [4, 1]


def is_five_of_a_kind(card_map):
    scored = add_jokers(card_map)
    return scored == [5]


def get_card_rank(card):
    return ['J', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'Q', 'K', 'A'].index(card)


def compare_cards(new, existing):
    new_rank = get_card_rank(new)
    existing_rank = get_card_rank(existing)

    if new_rank > existing_rank:
        return 1
    elif new_rank < existing_rank:
        return -1
    else:
        return 0


def compare_hands(new_hand, existing_hand):
    j = 0
    hand_result = None

    while hand_result is None:
        new_card = new_hand[j]
        existing_card = existing_hand[j]
        card_result = compare_cards(new_card, existing_card)

        if card_result == 0:
            j += 1
        else:
            hand_result = card_result

    return hand_result


def identify_index_for_new_hand(new_hand, all_hands):
    index = 0
    for [hand, bid] in all_hands:
        if compare_hands(new_hand, hand) == 1:
            index += 1
        else:
            break

    return index


def rank_hands(hands):
    ranked = { 0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [] }
    for hand in hands:
        [cards, bid] = split_cards_from_bid(hand)
        card_map = build_card_map(cards)
        sorted_card_map = sort_card_map_by_values(card_map)

        print(f'cards :: {cards}')

        if is_high_card(sorted_card_map):
            index = identify_index_for_new_hand(cards, ranked[0])
            ranked[0].insert(index, [cards, int(bid)])
        elif is_one_pair(sorted_card_map):
            index = identify_index_for_new_hand(cards, ranked[1])
            ranked[1].insert(index, [cards, int(bid)])
        elif is_two_pair(sorted_card_map):
            index = identify_index_for_new_hand(cards, ranked[2])
            ranked[2].insert(index, [cards, int(bid)])
        elif is_three_of_a_kind(sorted_card_map):
            index = identify_index_for_new_hand(cards, ranked[3])
            ranked[3].insert(index, [cards, int(bid)])
        elif is_full_house(sorted_card_map):
            index = identify_index_for_new_hand(cards, ranked[4])
            ranked[4].insert(index, [cards, int(bid)])
        elif is_four_of_a_kind(sorted_card_map):
            index = identify_index_for_new_hand(cards, ranked[5])
            ranked[5].insert(index, [cards, int(bid)])
        elif is_five_of_a_kind(sorted_card_map):
            index = identify_index_for_new_hand(cards, ranked[6])
            ranked[6].insert(index, [cards, int(bid)])
        else:
            raise Exception(f'Not sure how to rank: {cards}')

    return ranked
