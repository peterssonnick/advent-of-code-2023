from main import calculate_scratchcard_score, calculate_score_of_winning_matches, calculate_total_number_of_scratchcards

total_pt1 = 0
total_pt2 = 0

# pt. 1
with open('./inputs/input.txt') as f:
    scratchcards = f.readlines()
    for scratchcard in scratchcards:
        winners = calculate_scratchcard_score(scratchcard)
        score = calculate_score_of_winning_matches(winners)
        print(f'{score} :: {scratchcard}')
        total_pt1 += score

# pt. 2
with open('./inputs/input.txt') as f:
    scratchcards = f.readlines()
    total_pt2 += calculate_total_number_of_scratchcards(scratchcards)

print(f'pt. 1 :: {total_pt1}')
print(f'pt. 2 :: {total_pt2}')