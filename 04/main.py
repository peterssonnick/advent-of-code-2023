def split_card_number_from_line(line):
    return [part.strip() for part in line.split(':')]


def split_winning_numbers_from_line(line):
    return [part.strip() for part in line.split('|')]


def split_numbers_from_line(line):
    return [int(part.strip()) for part in line.split(' ') if part != '']


def identify_matching_winning_numbers(winning_numbers, game_numbers):
    return [number for number in game_numbers if number in winning_numbers]


def calculate_score_of_winning_matches(winning_matches):
    return 0 if len(winning_matches) == 0 else 2 ** (len(winning_matches) - 1)


def calculate_scratchcard_score(line):
    [card, numbers_string] = split_card_number_from_line(line)
    [winning_numbers_string, game_numbers_string] = split_winning_numbers_from_line(numbers_string)

    winning_numbers = split_numbers_from_line(winning_numbers_string)
    game_numbers = split_numbers_from_line(game_numbers_string)

    return identify_matching_winning_numbers(winning_numbers, game_numbers)


def identify_copies(scratchcards, starting_index, copies_won):
    start = starting_index + 1
    end = starting_index + 1 + copies_won
    return scratchcards[start:end]


def evaluate_scratchcard(scratchcards, scratchcard, i):
    score = 0
    winners = calculate_scratchcard_score(scratchcard)
    score += len(winners)

    copies = identify_copies(scratchcards, i, len(winners))
    for j, copy in enumerate(copies):
        copy_score = evaluate_scratchcard(scratchcards, copy, (i+j+1))
        score += copy_score

    return score


def calculate_total_number_of_scratchcards(scratchcards):
    total = 0

    for i, scratchcard in enumerate(scratchcards):
        score = evaluate_scratchcard(scratchcards, scratchcard, i)
        print(f'card {i+1} :: {score}')
        total += 1
        total += score

    return total
