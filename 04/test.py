from main import (
    split_card_number_from_line,
    split_winning_numbers_from_line,
    split_numbers_from_line,
    identify_matching_winning_numbers,
    calculate_score_of_winning_matches,
    calculate_scratchcard_score,
    identify_copies,
    calculate_total_number_of_scratchcards
)


def test_split_card_number_from_line():
    line = 'Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53'
    expected = ['Card 1', '41 48 83 86 17 | 83 86  6 31 17  9 48 53']
    actual = split_card_number_from_line(line)
    assert actual == expected


def test_split_winning_numbers_from_game_numbers():
    line = '41 48 83 86 17 | 83 86  6 31 17  9 48 53'
    expected = ['41 48 83 86 17', '83 86  6 31 17  9 48 53']
    actual = split_winning_numbers_from_line(line)
    assert actual == expected


def test_split_number_string_with_winning_numbers():
    line = '41 48 83 86 17'
    expected = [41, 48, 83, 86, 17]
    actual = split_numbers_from_line(line)
    assert actual == expected


def test_split_number_string_with_game_numbers():
    line = '83 86  6 31 17  9 48 53'
    expected = [83, 86, 6, 31, 17, 9, 48, 53]
    actual = split_numbers_from_line(line)
    assert actual == expected


def test_identify_winning_numbers():
    winning_numbers = [41, 48, 83, 86, 17]
    game_numbers = [83, 86, 6, 31, 17, 9, 48, 53]
    expected = [48, 83, 86, 17]
    actual = identify_matching_winning_numbers(winning_numbers, game_numbers)
    expected.sort()
    actual.sort()
    assert actual == expected


def test_calculate_score_of_winning_matches_with_0_matchs():
    winning_matches = []
    expected = 0
    actual = calculate_score_of_winning_matches(winning_matches)
    assert actual == expected


def test_calculate_score_of_winning_matches_with_1_match():
    winning_matches = [48]
    expected = 1
    actual = calculate_score_of_winning_matches(winning_matches)
    assert actual == expected


def test_calculate_score_of_winning_matches_with_4_matches():
    winning_matches = [48, 83, 86, 17]
    expected = 8
    actual = calculate_score_of_winning_matches(winning_matches)
    assert actual == expected


def test_can_calculate_scratchcard_score():
    line = 'Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53'
    expected = [48, 83, 86, 17]
    actual = calculate_scratchcard_score(line)
    expected.sort()
    actual.sort()
    assert actual == expected


def test_can_identify_copies():
    scratchcards = [
        "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
        "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
        "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
        "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
        "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
        "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"
    ]
    starting_index = 0
    copies_won = 4
    expected = [
        "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
        "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
        "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
        "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36"
    ]
    actual = identify_copies(scratchcards, starting_index, copies_won)
    assert actual == expected


def test_can_identify_copies_when_none_remain():
    scratchcards = [
        "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
        "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
        "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
        "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
        "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
        "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"
    ]
    starting_index = 4
    copies_won = 4
    expected = [
        "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"
    ]
    actual = identify_copies(scratchcards, starting_index, copies_won)
    assert actual == expected


def test_calculate_total_number_of_scratchcards():
    scratchcards = [
        "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
        "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
        "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
        "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
        "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
        "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"
    ]
    expected = 30
    actual = calculate_total_number_of_scratchcards(scratchcards)
    assert actual == expected