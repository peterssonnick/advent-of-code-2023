def split_seeds(line):
    return [int(seed) for seed in line.split(':')[1].split(' ') if seed.strip().isnumeric()]


def split_seed_ranges(seeds):
    curr_range = []
    ranges = []
    for seed in seeds:
        curr_range.append(seed)
        if len(curr_range) == 2:
            ranges.append(curr_range)
            curr_range = []
    return ranges

def split_category_name(line):
    return line.split(' ')[0]


def is_start_of_new_map(line):
    return 'map:' in line


def is_end_of_current_map(line):
    return '' == line.strip()


def parse_destination_source_and_range_length(line):
    return [int(num) for num in line.split(' ')]


def create_category_map_for_range(destination, source, range_length):
    return {
        "min": source,
        "max": (source + range_length) - 1,
        "destination": (destination - source)
    }


def identify_destination_in_category_map(category_map, source):
    for map_range in category_map:
        min = map_range.get('min')
        max = map_range.get('max')
        destination = map_range.get('destination')

        if min <= source <= max:
            return source + destination

    return source


def build_category_map(map_ranges):
    category_map = []

    for i, map_range in enumerate(map_ranges):
        print(f'beginning map for range {i}/{len(map_ranges)}')
        [destination, source, range_length] = parse_destination_source_and_range_length(map_range)
        map_for_range = create_category_map_for_range(destination, source, range_length)
        category_map.append(map_for_range)

    return category_map


def create_almanac(lines):
    almanac = {}
    current_category = ''
    current_ranges = []
    seeds = split_seeds(lines[0])
    almanac['seeds'] = seeds

    for line in lines[2:]:
        if is_start_of_new_map(line):
            current_category = split_category_name(line)
            print(f'starting building {current_category}')
        elif is_end_of_current_map(line):
            category_map = build_category_map(current_ranges)
            almanac[current_category] = category_map
            print(f'completed building {current_category}')

            current_ranges = []
            current_category = ''
        else:
            current_ranges.append(line)

    if len(current_ranges) > 0:
        almanac[current_category] = build_category_map(current_ranges)

    return almanac


def find_location_in_almanac_for_seed(almanac, seed):
    soil = identify_destination_in_category_map(almanac.get('seed-to-soil'), seed)
    fertilizer = identify_destination_in_category_map(almanac.get('soil-to-fertilizer'), soil)
    water = identify_destination_in_category_map(almanac.get('fertilizer-to-water'), fertilizer)
    light = identify_destination_in_category_map(almanac.get('water-to-light'), water)
    temperature = identify_destination_in_category_map(almanac.get('light-to-temperature'), light)
    humidity = identify_destination_in_category_map(almanac.get('temperature-to-humidity'), temperature)
    location = identify_destination_in_category_map(almanac.get('humidity-to-location'), humidity)

    return location

