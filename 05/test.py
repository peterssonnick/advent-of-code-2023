from main import (
    parse_destination_source_and_range_length,
    create_category_map_for_range,
    identify_destination_in_category_map,
    build_category_map,
    create_almanac,
    split_seeds,
    split_category_name,
    is_start_of_new_map,
    is_end_of_current_map,
    split_seed_ranges
)


def test_parse_destination_source_and_range():
    line = '50 98 2'
    [destination, source, range_length] = parse_destination_source_and_range_length(line)
    assert destination == 50
    assert source == 98
    assert range_length == 2


def test_create_category_map_for_range():
    destination = 50
    source = 98
    range_length = 2

    expected = {
        "min": 98,
        "max": 99,
        "destination": -48
    }
    actual = create_category_map_for_range(destination, source, range_length)
    assert actual == expected


def test_identify_destination_in_category_map_for_source_in_map():
    target = 98
    category_map = [{
        'min': 98,
        'max': 99,
        'destination': -48
    }]
    expected = 50
    actual = identify_destination_in_category_map(category_map, target)
    assert actual == expected


def test_identify_destination_in_category_map_for_source_not_in_map():
    source = 97
    category_map = {
        98: 50,
        99: 51
    }
    expected = 97
    actual = identify_destination_in_category_map(category_map, source)
    assert actual == expected


def test_build_category_map():
    ranges = ['50 98 2', '52 50 3']
    expected = [{
        'min': 98,
        'max': 99,
        'destination': -48
    },{
        'min': 50,
        'max': 52,
        'destination': 2
    }]
    actual = build_category_map(ranges)
    assert actual == expected


def test_split_seeds():
    line = 'seeds: 79 14 55 13'
    expected = [79, 14, 55, 13]
    actual = split_seeds(line)
    assert actual == expected


def test_split_category_name():
    line = 'seed-to-soil map:'
    expected = 'seed-to-soil'
    actual = split_category_name(line)
    assert actual == expected


def test_is_start_of_new_map():
    line = 'seed-to-soil map:'
    assert is_start_of_new_map(line) is True


def test_is_end_of_current_map():
    line = '\n'
    assert is_end_of_current_map(line) is True


def test_create_almanac():
    lines = [
        'seeds: 79 14 55 13',
        '',
        'seed-to-soil map:',
        '50 98 2',
        '52 50 3',
        ''
    ]

    expected = {
        "seeds": [79, 14, 55, 13],
        "seed-to-soil": [{
            'min': 98,
            'max': 99,
            'destination': -48
        },{
            'min': 50,
            'max': 52,
            'destination': 2
        }]
    }

    actual = create_almanac(lines)
    assert actual == expected


def test_split_seed_ranges():
    seed_ranges = [79, 14, 55, 13]
    expected = [[79, 14], [55, 13]]
    actual = split_seed_ranges(seed_ranges)
    assert actual == expected