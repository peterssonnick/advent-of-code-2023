from main import (
    create_almanac,
    find_location_in_almanac_for_seed,
    split_seed_ranges
)

# pt. 1
# with open('./inputs/input.txt') as f:
#     lines = f.readlines()
#     almanac = create_almanac(lines)
#     locations = []
#
#     for seed in almanac.get('seeds'):
#         location = find_location_in_almanac_for_seed(almanac, seed)
#         locations.append(location)


# pt. 2
with open('./inputs/sample.txt') as f:
    lines = f.readlines()
    almanac = create_almanac(lines)
    seed_ranges = split_seed_ranges(almanac.get('seeds'))
    locations = []

    for i, seed_range in enumerate(seed_ranges):
        start = seed_range[0]
        end = seed_range[0] + seed_range[1]

        for j in range(end - start):
            seed = j + start
            location = find_location_in_almanac_for_seed(almanac, seed)
            locations.append(location)

locations.sort()
print(locations)
