from main import (
    parse_numbers_from_line,
    character_is_symbol,
    identify_indexes_of_symbols_in_line,
    identify_nth_start_end_index_of_number_in_line,
    identify_numbers_adjacent_to_symbol_in_line,
    identify_occurrence_of_number_in_line,
    identify_indexes_of_potential_gear_symbols_in_line,
    identify_gear_ratios_in_line
)


def test_parse_numbers_from_line_returns_numbers_in_line():
    line = '467..114..'
    expected = ['467', '114']
    actual = parse_numbers_from_line(line)
    assert actual == expected


def test_parse_numbers_from_line_with_adjacent_symbols():
    line = '617*......'
    expected = ['617']
    actual = parse_numbers_from_line(line)
    assert actual == expected


def test_parse_numbers_from_line_returns_empty_array_when_no_numbers_in_line():
    line = '...*......'
    expected = []
    actual = parse_numbers_from_line(line)
    assert actual == expected


def test_can_identify_symbol():
    assert character_is_symbol('.') is False
    assert character_is_symbol('467') is False
    assert character_is_symbol('*') is True
    assert character_is_symbol('@') is True
    assert character_is_symbol('$') is True
    assert character_is_symbol('=') is True
    assert character_is_symbol('%') is True


def test_parse_indexes_of_symbols_in_line():
    line = '...$.*....'
    expected = [3, 5]
    actual = identify_indexes_of_symbols_in_line(line)
    assert actual == expected


def test_can_get_nth_start_end_index_of_number_in_line_when_n_is_0():
    line = '617*..617.'
    number = '617'
    n = 0
    expected = [0, 2]
    actual = identify_nth_start_end_index_of_number_in_line(line, number, n)
    assert actual == expected


def test_can_get_nth_start_end_index_of_number_in_line_when_n_is_1():
    line = '617*..617.'
    number = '617'
    n = 1
    expected = [6, 8]
    actual = identify_nth_start_end_index_of_number_in_line(line, number, n)
    assert actual == expected


def test_can_get_nth_start_end_index_of_number_in_line_when_n_is_1_and_number_is_single_character():
    line = '617*..6.'
    number = '6'
    n = 1
    expected = [6, 6]
    actual = identify_nth_start_end_index_of_number_in_line(line, number, n)
    assert actual == expected



def test_identify_numbers_adjacent_to_symbol_in_line():
    prev_line = ''
    curr_line = '617*......'
    next_line = ''
    expected = [617]
    actual = identify_numbers_adjacent_to_symbol_in_line(prev_line, curr_line, next_line)
    assert actual == expected


def test_identify_numbers_adjacent_to_symbol_in_line_when_multiple_matches_exist_for_number():
    prev_line = ''
    curr_line = '617...61*.'
    next_line = ''
    expected = [61]
    actual = identify_numbers_adjacent_to_symbol_in_line(prev_line, curr_line, next_line)
    assert actual == expected


def test_identify_numbers_adjacent_to_symbol_in_previous_line_and_next_line():
    prev_line = '.....*..#................506..143........375......77.....155...........400.518...64....773...718..797........694....972.603.....*...........'
    curr_line = '....479.795...............*..........800...........*.$.......264*636.......@..............&..*...*.......499...............*...5.20.........'
    next_line = '515...................512.484...*....*...=......390...427...................................644.804.........*...@......-..532............28.'
    expected = [479, 795, 800, 264, 636, 499, 5, 20]
    actual = identify_numbers_adjacent_to_symbol_in_line(prev_line, curr_line, next_line)
    assert actual == expected


def test_get_match_number_of_number_in_line():
    numbers = ['617', '27', '61']
    target = '61'
    target_index = 2
    expected = 1
    actual = identify_occurrence_of_number_in_line(numbers, target, target_index)
    assert actual == expected


def test_identify_indexes_of_potential_gear_symbols_in_line():
    line = '617*......'
    expected = [3]
    actual = identify_indexes_of_potential_gear_symbols_in_line(line)
    assert actual == expected


def test_identify_gear_ratio_in_line():
    prev_line = '467..114..'
    curr_line = '...*......'
    next_line = '..35..633.'
    expected = [[467,35]]
    actual = identify_gear_ratios_in_line(prev_line, curr_line, next_line)
    assert actual == expected

def test_identify_gear_ratio_in_line():
    prev_line = '...*..982.....657..70=..948#..159..777../.....757....................816...............887.............325........*....................871..'
    curr_line = '.804..........=.....................*..284..........255*...............+....48...589*......199................#.68..542......&.&241...*.....'
    next_line = '.......10............604.....287...66......507..........917...585............-.......6.327..-......822.....718.....*.......825.......34.....'
    expected = [(777*66), (255*917), (589*6), (871*34)]
    actual = identify_gear_ratios_in_line(prev_line, curr_line, next_line)
    assert actual == expected


def test_identify_nth_start_end_index_of_number_in_line_many_6s():
    numbers = ['10', '604', '287', '66', '507', '917', '585', '6', '327', '822', '718', '825', '34']
    number = '6'
    i = 7
    expected = 3
    actual = identify_occurrence_of_number_in_line(numbers, number, i)
    assert actual == expected

