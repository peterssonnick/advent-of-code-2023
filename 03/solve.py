from main import (
    identify_numbers_adjacent_to_symbol_in_line,
    identify_gear_ratios_in_line
)

sum_of_part_numbers = 0
sum_of_gear_ratios = 0

with open('./inputs/input.txt') as f:
# with open('./inputs/sample.txt') as f:
    lines = f.readlines()
    for i, line in enumerate(lines):
        prev_line = '' if i == 0 else lines[i - 1].strip()
        curr_line = line.strip()
        next_line = '' if i == (len(lines) - 1) else lines[i + 1].strip()

        # pt 1
        adjacent_numbers = identify_numbers_adjacent_to_symbol_in_line(prev_line, curr_line, next_line)
        sum_of_part_numbers += sum(adjacent_numbers)

        # pt 2
        gear_ratios = identify_gear_ratios_in_line(prev_line, curr_line, next_line)
        sum_of_gear_ratios += sum(gear_ratios)

        print(f'*******{i}*******')

        print(f'adjacent numbers :: {adjacent_numbers} :: {sum(adjacent_numbers)}')
        print(f'gear_ratios :: {gear_ratios}')

        print(f'prev line :: {prev_line}')
        print(f'curr line :: {curr_line}')
        print(f'next line :: {next_line}')
        print(f'*******{i}*******\n\n')

        if next_line == '':
            break

print(f'pt. 1 ::{sum_of_part_numbers}')
print(f'pt. 2 :: {sum_of_gear_ratios}')
