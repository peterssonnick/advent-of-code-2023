def parse_numbers_from_line(line):
    numbers = []
    current_number = ''
    for char in line:
        if char.isnumeric():
            current_number += char
        else:
            if current_number != '':
                numbers.append(current_number)
                current_number = ''

    if current_number != '':
        numbers.append(current_number)
    return numbers


def character_is_symbol(character):
    return not character.isnumeric() and character != '.' and character != ''


def identify_indexes_of_symbols_in_line(line):
    return [i for i, symbol in enumerate(line) if character_is_symbol(symbol)]


def identify_indexes_of_potential_gear_symbols_in_line(line):
    return [i for i, character in enumerate(line) if character == '*']


def identify_nth_start_end_index_of_number_in_line(line, number, n):
    match_number = 0
    search_start_index = 0
    while match_number <= n:
        start_index = line.find(number, search_start_index)
        end_index = start_index + (len(number) - 1)

        if match_number == n:
            break
        else:
            match_number += 1
            search_start_index = (end_index + 1)

    return [start_index, end_index]


def symbol_index_is_adjacent_to_number(start_index, end_index, symbol_index):
    return (symbol_index >= start_index - 1) and (symbol_index <= end_index + 1)


def identify_occurrence_of_number_in_line(numbers, target, target_index):
    match_number = 0
    for i, number in enumerate(numbers):
        if target in number and i != target_index:
            match_number += number.count(target)
        elif target_index == i:
            break
    return match_number


def identify_numbers_adjacent_to_symbol_in_line(prev_line, curr_line, next_line):
    numbers_adjacent_to_symbol = []

    numbers = parse_numbers_from_line(curr_line)
    symbol_indexes = identify_indexes_of_symbols_in_line(curr_line)
    symbol_indexes += identify_indexes_of_symbols_in_line(prev_line)
    symbol_indexes += identify_indexes_of_symbols_in_line(next_line)

    for i, number in enumerate(numbers):
        n = identify_occurrence_of_number_in_line(numbers, number, i)
        [start_index, end_index] = identify_nth_start_end_index_of_number_in_line(curr_line, number, n)

        for symbol_index in symbol_indexes:
            if symbol_index_is_adjacent_to_number(start_index, end_index, symbol_index):
                numbers_adjacent_to_symbol.append(int(number))
    return numbers_adjacent_to_symbol


def identify_numbers_adjacent_to_gear_symbol(line, gear_symbol_index):

    adjacent_numbers = []
    numbers = parse_numbers_from_line(line)

    for i, number in enumerate(numbers):
        n = identify_occurrence_of_number_in_line(numbers, number, i)
        [start_index, end_index] = identify_nth_start_end_index_of_number_in_line(line, number, n)

        if symbol_index_is_adjacent_to_number(start_index, end_index, gear_symbol_index):
            adjacent_numbers.append(int(number))

    return adjacent_numbers


def identify_gear_ratios_in_line(prev_line, curr_line, next_line):
    gear_ratios = []

    potential_gear_symbol_indexes = identify_indexes_of_potential_gear_symbols_in_line(curr_line)

    for potential_gear_symbol_index in potential_gear_symbol_indexes:
        adjacent_numbers = identify_numbers_adjacent_to_gear_symbol(prev_line, potential_gear_symbol_index)
        adjacent_numbers += identify_numbers_adjacent_to_gear_symbol(curr_line, potential_gear_symbol_index)
        adjacent_numbers += identify_numbers_adjacent_to_gear_symbol(next_line, potential_gear_symbol_index)

        if len(adjacent_numbers) == 2:
            gear_ratios.append(adjacent_numbers[0]*adjacent_numbers[1])

    return gear_ratios

