class BinaryTreeNode:
    def __init__(self, data):
        self.data = data
        self.leftChild = None
        self.rightChild = None

    def __eq__(self, other):
        if isinstance(other, BinaryTreeNode):
            return (self.data == other.data
                    and self.rightChild == other.rightChild
                    and self.leftChild == other.leftChild)
        return False


def split_values_in_line(line):
    return [int(value) for value in line.split(' ')]


def create_base_nodes(values):
    return [BinaryTreeNode(value) for value in values]


def create_parent_node(left_node, right_node):
    parent = BinaryTreeNode(right_node.data - left_node.data)
    parent.leftChild = left_node
    parent.rightChild = right_node
    return parent


def nodes_are_all_zeros(nodes):
    return set([node.data for node in nodes]) == set([0])


def create_parent_nodes(base_nodes):
    parent_nodes = []
    for i in range(len(base_nodes) - 1):
        left_node = base_nodes[i]
        right_node = base_nodes[i+1]
        parent_node = create_parent_node(left_node, right_node)
        parent_nodes.append(parent_node)
    return parent_nodes


def create_tree(line):
    values = split_values_in_line(line)
    base_nodes = create_base_nodes(values)
    parent_nodes = create_parent_nodes(base_nodes)

    while not nodes_are_all_zeros(parent_nodes):
        parent_nodes = create_parent_nodes(parent_nodes)

    return parent_nodes


def get_next_value_in_line(line):
    tree = create_tree(line)
    node = tree[len(tree) - 1]
    complete = False
    children = []

    while not complete:
        if node.rightChild:
            children.append(node.rightChild.data)
            node = node.rightChild
        else:
            complete = True

    return sum(children)


def traverse_children(children):
    result = 0
    for child in children:
        result = child - result
    return result


def get_previous_value_in_line(line):
    tree = create_tree(line)
    node = tree[0]
    complete = False
    children = []

    while not complete:
        if node.leftChild:
            children.append(node.leftChild.data)
            node = node.leftChild
        else:
            complete = True

    return traverse_children(children)
