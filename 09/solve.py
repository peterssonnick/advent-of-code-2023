from main import (
    get_next_value_in_line,
    get_previous_value_in_line
)

with open('./inputs/input.txt') as f:
# with open('./inputs/sample.txt') as f:
    lines = f.readlines()
    sum = 0

    # pt. 1
    # for i, line in enumerate(lines):
    #     next = get_next_value_in_line(line)
    #     print(f'{line}: {next}')
    #     sum += next

    # pt. 2:
    for i, line in enumerate(lines):
        prev = get_previous_value_in_line(line)
        print(f'{line}: {prev}')
        sum += prev

    print(f'total: {sum}')