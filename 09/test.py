from main import (
    BinaryTreeNode,
    split_values_in_line,
    create_base_nodes,
    nodes_are_all_zeros,
    get_next_value_in_line,
    create_parent_node,
    create_tree,
    traverse_children
)


def test_create_node():
    node = BinaryTreeNode(3)
    assert node.data == 3
    assert node.leftChild is None
    assert node.rightChild is None


def test_split_values_in_line():
    line = '25 43 75 138 255 455 773 1250'
    expected = [25, 43, 75, 138, 255, 455, 773, 1250]
    actual = split_values_in_line(line)
    assert actual == expected


def test_create_nodes():
    values = [25, 43, 75]
    node_1 = BinaryTreeNode(25)
    node_2 = BinaryTreeNode(43)
    node_3 = BinaryTreeNode(75)
    expected = [node_1, node_2, node_3]
    actual = create_base_nodes(values)
    assert actual == expected


def test_create_parent_node():
    left_node = BinaryTreeNode(25)
    right_node = BinaryTreeNode(43)
    expected = BinaryTreeNode(18)
    expected.leftChild = left_node
    expected.rightChild = right_node
    actual = create_parent_node(left_node, right_node)
    assert actual == expected


def test_nodes_are_all_zeros():
    assert nodes_are_all_zeros([]) is False
    assert nodes_are_all_zeros([BinaryTreeNode(0), BinaryTreeNode(0)]) is True
    assert nodes_are_all_zeros([BinaryTreeNode(0), BinaryTreeNode(1)]) is False


def test_create_tree():
    line = '0 3 6'
    node_1 = BinaryTreeNode(0)
    node_2 = BinaryTreeNode(3)
    node_3 = BinaryTreeNode(6)

    node_4 = BinaryTreeNode(node_2.data - node_1.data)
    node_4.leftChild = node_1
    node_4.rightChild = node_2

    node_5 = BinaryTreeNode(node_3.data - node_2.data)
    node_5.leftChild = node_2
    node_5.rightChild = node_3

    node_6 = BinaryTreeNode(node_5.data - node_4.data)
    node_6.leftChild = node_4
    node_6.rightChild = node_5

    expected = [node_6]
    actual = create_tree(line)
    assert actual == expected


def test_get_next_value_in_line():
    line = '0 3 6'
    expected = 9
    actual = get_next_value_in_line(line)
    assert actual == expected


def test_traverse_children():
    expected = 5
    children = [2, 0, 3, 10]
    actual = traverse_children(children)
    assert actual == expected
