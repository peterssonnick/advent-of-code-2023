from main import parse_first_and_last_digit

input_file_name = 'inputs/input.txt'

with open(input_file_name) as f:
    result = 0
    lines = f.readlines()

    for line in lines:
        result += parse_first_and_last_digit(line)

print(f'result :: {result}')