_key_string_conversion = {
    '1': 1, 'one': 1,
    '2': 2, 'two': 2,
    '3': 3, 'three': 3,
    '4': 4, 'four': 4,
    '5': 5, 'five': 5,
    '6': 6, 'six': 6,
    '7': 7, 'seven': 7,
    '8': 8, 'eight': 8,
    '9': 9, 'nine': 9
}


def build_index_map(keys, string):
    index_map = {}

    for key in keys:
        index = string.find(key)
        if index != -1:
            index_map[index] = key

    return index_map


def find_first_key_in_string(keys, string):
    index_map = build_index_map(keys, string)
    index_map_keys = list(index_map.keys())
    index_map_keys.sort()

    return index_map[index_map_keys[0]]


def parse_first_and_last_digit(string):
    keys = _key_string_conversion.keys()
    reversed_keys = [key[::-1] for key in keys]

    first_key = find_first_key_in_string(keys, string)
    last_key = find_first_key_in_string(reversed_keys, string[::-1])

    first_key_int_value = _key_string_conversion.get(first_key)
    last_key_int_value = _key_string_conversion.get(last_key[::-1])

    print(f'{string} :: {first_key_int_value}{last_key_int_value}')

    return int(f'{first_key_int_value}{last_key_int_value}')
