from main import parse_first_and_last_digit


def test_can_parse_first_and_last_digit_1abc2():
    line = '1abc2'
    result = parse_first_and_last_digit(line)
    assert result == 12


def test_can_parse_first_and_last_digit_pqr3stu8vwx():
    line = 'pqr3stu8vwx'
    result = parse_first_and_last_digit(line)
    assert result == 38


def test_can_parse_first_and_last_digit_a1b2c3d4e5f():
    line = 'a1b2c3d4e5f'
    result = parse_first_and_last_digit(line)
    assert result == 15


def test_can_parse_first_and_last_digit_treb7uchet():
    line = 'treb7uchet'
    result = parse_first_and_last_digit(line)
    assert result == 77


def test_can_parse_first_and_last_digit_two1nine():
    line = 'two1nine'
    result = parse_first_and_last_digit(line)
    assert result == 29


def test_can_parse_first_and_last_digit_eightwothree():
    line = 'eightwothree'
    result = parse_first_and_last_digit(line)
    assert result == 83


def test_can_parse_first_and_last_digit_abcone2threexyz():
    line = 'abcone2threexyz'
    result = parse_first_and_last_digit(line)
    assert result == 13


def test_can_parse_first_and_last_digit_xtwone3four():
    line = 'xtwone3four'
    result = parse_first_and_last_digit(line)
    assert result == 24


def test_can_parse_first_and_last_digit_4nineeightseven2():
    line = '4nineeightseven2'
    result = parse_first_and_last_digit(line)
    assert result == 42


def test_can_parse_first_and_last_digit_zoneight234():
    line = 'zoneight234'
    result = parse_first_and_last_digit(line)
    assert result == 14


def test_can_parse_first_and_last_digit_7pqrstsixteen():
    line = '7pqrstsixteen'
    result = parse_first_and_last_digit(line)
    assert result == 76


def test_can_parse_first_and_last_digit_1oneninegspfm3four43():
    line = '1oneninegspfm3four43'
    result = parse_first_and_last_digit(line)
    assert result == 13


